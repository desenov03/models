import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.semi_supervised import LabelPropagation
from sklearn.preprocessing import OneHotEncoder

data = pd.DataFrame({
    "Name": ["Daniyar", "Damir", "Fariza", "Zhanibek", "Adilet", "Dossymzhan", "Meirzhan", "Erkenaz", "Nurmakhan", "Amanzhol", "Aknyr"],
    "Gender": ["M", "M", "F", "M", "M", "M", "M", "F", "M", "M", "F"],
    "Average GPA": [3.72, 3.5, 2.8, 3.5, 3.5, 3.77, 2.0, 3.51, 3.0, 2.8, 3.5],
    "Has Scholarship": [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    "High Scholarship": [1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1]  # Target variable (1 - receives, 0 - does not receive)
})


data = pd.get_dummies(data, columns=["Gender"], prefix=["Gender"])


X_labeled = data[["Gender_F", "Gender_M", "Average GPA", "Has Scholarship"]]
y_labeled = data["High Scholarship"]


X_unlabeled = pd.DataFrame({
    "Gender_F": [1, 0],
    "Gender_M": [0, 1],
    "Average GPA": [2.5, 3.2],
    "Has Scholarship": [0, 1]
})

X_combined = pd.concat([X_labeled, X_unlabeled])
y_combined = pd.concat([y_labeled, pd.Series([-1] * len(X_unlabeled))])  

X_train, X_test, y_train, y_test = train_test_split(X_combined, y_combined, test_size=0.2, random_state=42)

model = LabelPropagation(kernel='knn', n_neighbors=10)
model.fit(X_train, y_train)

y_pred = model.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy of Semi-Supervised Learning for Scholarship Classification: {accuracy}")
