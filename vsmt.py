import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import OneHotEncoder

data = pd.DataFrame({
    "Имя": ["Данияр", "Дамир", "Фариза", "Жанибек", "Адилет", "Досымжан", "Меиржан","Еркеназ","Нурмахан","Аманжол", "Акнур"],
    "Пол": ["М", "М", "Ж", "М", "М", "М","М", "Ж", "М","М","Ж"],
    "Средний GPA": [3.72, 3.5, 2.8, 3.5, 3.5, 3.77, 2.0, 3.51, 3.0, 2.8, 3.5],
    "Имеет стипендию": [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    "Повышенная стипендия": [1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1]  
})


encoder = OneHotEncoder(sparse=False)
gender_encoded = encoder.fit_transform(data[["Пол"]])
data_encoded = pd.concat([data, pd.DataFrame(gender_encoded, columns=encoder.get_feature_names_out(["Пол"]))], axis=1)
data_encoded = data_encoded.drop(columns=["Пол"])

X = data_encoded[["Средний GPA", "Имеет стипендию", "Пол_Ж", "Пол_М"]]
y = data_encoded["Повышенная стипендия"]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

svm_tree_classifier = DecisionTreeClassifier()

svm_tree_classifier.fit(X_train, y_train)

y_pred = svm_tree_classifier.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy of SVM with Decision Tree for Scholarship Classification: {accuracy}")
