import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder

# Ваш датасет
data = pd.DataFrame({
    "Имя": ["Данияр", "Дамир", "Фариза", "Жанибек", "Адилет", "Досымжан", "Меиржан", "Еркеназ", "Нурмахан", "Аманжол", "Акнур"],
    "Пол": ["М", "М", "Ж", "М", "М", "М", "М", "Ж", "М", "М", "Ж"],
    "Средний GPA": [3.72, 3.5, 2.8, 3.5, 3.5, 3.77, 2.0, 3.51, 3.0, 2.8, 3.5],
    "Имеет стипендию": [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    "Повышенная стипендия": [1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1]  # Целевая переменная (1 - получает, 0 - не получает)
})


label_encoder = LabelEncoder()
data["Пол"] = label_encoder.fit_transform(data["Пол"])

X = data[["Пол", "Средний GPA", "Имеет стипендию"]]
y = data["Повышенная стипендия"]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


svm_classifier = SVC()


svm_classifier.fit(X_train, y_train)

y_pred = svm_classifier.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy of SVM for Scholarship Classification: {accuracy}")
